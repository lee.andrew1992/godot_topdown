extends Area2D

signal shoot

export (PackedScene) var bullet

export (bool) var single_handed
export (float) var reload_timer
export (float) var shot_cooldown
export (float) var damage
export (float) var bullet_life_time
export (bool) var can_shoot
export (float) var ammo_count
export (float) var max_ammo 

var reloading

func _ready():
	ammo_count = max_ammo
	$ShotTimer.wait_time = shot_cooldown
	var bullet_creator = get_tree().get_root().get_child(0).get_node("Settings/BulletCreator") 
	connect("shoot", bullet_creator, "_on_shoot")
	reloading = false
	
func shoot():
	if ammo_count > 0 and not reloading:
		ammo_count -= 1	
		var muzzlePosition = $Muzzle.global_position
		var direction = (get_parent().global_position - muzzlePosition).normalized()
		emit_signal("shoot", bullet, muzzlePosition, -direction)
		start_shot_timer()

func start_shot_timer():
	$ShotTimer.start()

func _on_ShotTimer_timeout():
	can_shoot = true

func reload(reload_timer):
	reloading = true
	$ReloadTimer.wait_time = reload_timer
	$ReloadTimer.start()

func _on_ReloadTimer_timeout():
	reloading = false
	$ReloadTimer.stop()
	ammo_count = max_ammo

func empty():
	return ammo_count == 0