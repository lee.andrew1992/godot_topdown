extends Node2D

func _ready():
	pass
	
func _on_shoot(bullet, _position, _direction):
	var new_bullet = bullet.instance()
	add_child(new_bullet)
	new_bullet.start(_position, _direction)