extends Area2D

export (float) var speed
export (float) var damage 
export (float) var lifeTime

var velocity = Vector2()
var direction = Vector2()

func start (_position, _direction):
	position = _position
	rotation = _direction.angle()
	$LifeTime.wait_time = lifeTime
	velocity = _direction * speed
	direction = _direction

func _process(delta):
	position += velocity * delta

func explode():
	queue_free()

func _on_Bullet_body_entered(body):
	explode()
	if body.has_method("take_damage"):
		body.take_damage(damage)
		
	if body.get_collision_layer() == 16: # wall / door layer == 16
		body.get_parent().create_debris()
		
	if body.get_collision_layer() == 32:
		#if it is not open, open it
		if (body.state != 0):
			body.open_door(direction)
		
		#if it is already open, close it
		else:
			body.close_door(direction)


func _on_LifeTime_timeout():
	explode()
