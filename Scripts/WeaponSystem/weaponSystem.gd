extends Node2D

var inventory
var weapon_type
var equipped

var body

# WEAPON_TYPE
# knife = 0
# singleHanded = 1
# dualHanded = 2
# twoHanded = 3

# WEAPONS
# none = 0
# pistol = 1
# smg = 2

func _ready():
	body = get_parent().get_node("Body")
	
	inventory = get_parent().inventory
	weapon_type = get_parent().weapon_type
	equipped = get_parent().equipped

	init_inventory()
	equip_weapon()
	update_equipped_graphics()

func init_inventory():
	for index in range(0, inventory.size()):
		if (inventory[index] != null):
			inventory[index] = inventory[index].instance()

func equip_weapon():
	match (weapon_type):
		1: # single handed
			equipped[0] = inventory[0]
			equipped[1] = null
		
		2: # dual handed
			equipped[0] = inventory[0]
			equipped[1] = inventory[1]
			
		3: #two handed
			equipped[0] = inventory[2]
			equipped[1] = null

func update_equipped_graphics():
	match (weapon_type):
		1: # single handed
			pass
			
		2: # dual handed
			var right_arm = body.get_child(1)
			var left_arm = body.get_child(0)

			left_arm.get_node("Hand").add_child(equipped[1])
			right_arm.get_node("Hand").add_child(equipped[0])
			
		3: # two handed
			pass