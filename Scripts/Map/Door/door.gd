extends "res://Scripts/Map/Wall/wall.gd"

enum DOOR_STATE {
	opened = 0,
	closed = 1,
	locked = 2
}

enum DOOR_PIVOT_LOC {
	left = 0,
	right = 1	
}

export (DOOR_STATE) var state
export (DOOR_PIVOT_LOC) var pivotType

func _ready():
	state = DOOR_STATE.closed

func open_door(bullet_dir):
	if (bullet_dir.x > 0):	
		if (pivotType == DOOR_PIVOT_LOC.left):
			get_parent().rotation_degrees = -90
		else:
			get_parent().rotation_degrees = 90
		state = DOOR_STATE.opened
		
	elif (bullet_dir.x < 0):
		if (pivotType == DOOR_PIVOT_LOC.left):
			get_parent().rotation_degrees = 90
		else:
			get_parent().rotation_degrees = -90	
		state = DOOR_STATE.opened
		
func close_door(bullet_dir):
	if (pivotType == DOOR_PIVOT_LOC.left):
		if (bullet_dir.y > 0):
			get_parent().rotation_degrees = 0
			state = DOOR_STATE.closed
			
	elif (pivotType == DOOR_PIVOT_LOC.right):
		if (bullet_dir.y < 0):
			get_parent().rotation_degrees = 0
			state = DOOR_STATE.closed