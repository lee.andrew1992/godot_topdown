extends Node2D

var game_settings
var cover_node_manager
var target_points = []

var attack_score = 0
var safe_score = 0
var occupied

onready var common_functions = preload("res://Scripts/Common/common.gd").new()

func _ready():
	game_settings = get_tree().get_root().get_child(0).get_node("Settings/GameController") 
	cover_node_manager = get_tree().get_root().get_child(0).get_node("Map")
	var sprite = get_child(0)
	sprite.visible = game_settings.debug_mode
	occupied = false
	
func _draw():
	if game_settings.debug_mode:
		if len(target_points) > 0 and occupied:
			for target_point in target_points:			
				draw_line(Vector2(), target_point - self.global_position, Color(0,0,1), 1)

func _on_WallChecker_body_entered(body):
	if body.get_collision_layer() == 16: # wall
		queue_free()
		cover_node_manager.remove_cover_node(self)
		pass
		
func _process(delta):
	update()
		
func set_node_score(player): 
	target_points = common_functions.target_points(self.get_node("WallChecker"), player)
	attack_score = len(target_points)
	safe_score = player.get_global_position().distance_to(player.global_position) - len(target_points)
	
func release_node():
	target_points = []
	occupied = false