extends Node2D

enum MAT_TYPE {	
	brick = 0,
	wood = 1,
	glass = 2
}

export (MAT_TYPE) var wallType
export (Color) var dominantColor
export (PackedScene) var cover_node

var cover_points = []
var hide_points = []

onready var cover_node_manager = get_tree().get_root().get_child(0).get_node("Map")  

func _ready():
	create_cover_nodes()

func create_debris():
	pass
	
func create_cover_nodes():
	var shape_extents = get_child(0).get_child(0).shape.extents
	var inner_corner_ns = Vector2 (1 / scale.x, 30 / scale.y)
	var inner_corner_we = Vector2 (30 / scale.x, 10 / scale.y)
	
	var wall_nw = -shape_extents + Vector2 (inner_corner_ns.x, -inner_corner_ns.y)
	var wall_ne = Vector2 (shape_extents.x, -shape_extents.y) - inner_corner_ns
	
	var wall_wn = -shape_extents + Vector2 (-inner_corner_we.x, inner_corner_we.y)
	var wall_ws = Vector2 (-shape_extents.x, shape_extents.y) - inner_corner_we
	
	var wall_sw = Vector2 (-shape_extents.x, shape_extents.y) + inner_corner_ns
	var wall_se = shape_extents - Vector2 (inner_corner_ns.x, -inner_corner_ns.y)
	
	var wall_en = shape_extents - Vector2 (-inner_corner_we.x, inner_corner_we.y)
	var wall_es = Vector2 (shape_extents.x, -shape_extents.y) + inner_corner_we
	
	var wall_n = Vector2 (0, -(shape_extents.y + inner_corner_ns.y))
	var wall_s = Vector2 (0, (shape_extents.y + inner_corner_ns.y))
	var wall_e = Vector2 ((shape_extents.x + inner_corner_we.x), 0)
	var wall_w = Vector2 (-(shape_extents.x + inner_corner_we.x), 0)
	
	for point in [wall_nw, wall_ne]:
		set_node(point, cover_points)
		
	for point in [wall_wn, wall_ws]:
		set_node(point, cover_points)

	for point in [wall_en, wall_es]:
		set_node(point, cover_points)
		
	for point in [wall_sw, wall_se]:
		set_node(point, cover_points)
		
	set_node(wall_n, hide_points)
	set_node(wall_w, hide_points)
	set_node(wall_e, hide_points)
	set_node(wall_s, hide_points)

func set_node(node_position, node_type_array):
	var new_cover_node = create_node(node_position)
	node_type_array.append(new_cover_node)
	return new_cover_node

func create_node(node_position):
	var new_cover_node = cover_node.instance()
	add_child(new_cover_node)
	cover_node_manager.add_cover_node(new_cover_node)
	new_cover_node.position = node_position
	new_cover_node.scale = Vector2 (1 / scale.x, 1 / scale.y) 
	
	return new_cover_node
	
func get_possible_cover_nodes(player):
	for node in cover_points:
		if is_instance_valid(node):
			node.set_node_score(player)
		
	for node in hide_points:
		if is_instance_valid(node):
			node.set_node_score(player)