extends Node2D

var tile_size = Vector2 (64, 64)
onready var texture = $Sprite.texture

func _ready():
	var tex_width = texture.get_width() / tile_size.x
	var tex_height = texture.get_height() / tile_size.y
	var tileSet = TileSet.new()
	for x in range(tex_width):
		for y in range(tex_height):
			var region = Rect2(x * tile_size.x, y * tile_size.y, tile_size.x, tile_size.y)
			var id = x + y * 10
			tileSet.create_tile(id)
			tileSet.tile_set_texture(id, texture)
			tileSet.tile_set_region(id, region)
	ResourceSaver.save("res://Scenes/TerrainTiles/terrain_tiles2.tres", tileSet)