extends KinematicBody2D

signal health_changed
signal dead

export (int) var speed
export (int) var health

enum WEAPON_TYPE{
	knife = 0
	singleHanded = 1,
	dualHanded = 2,
	twoHanded = 3
}

enum WEAPONS{
	none = 0
	pistol = 1
	smg = 2
}

export (WEAPON_TYPE) var weapon_type
export (Array, PackedScene) var inventory
export (Array, PackedScene) var equipped

var velocity = Vector2()
var alive = true

var game_settings
var enemy_settings
onready var common_functions = preload("res://Scripts/Common/common.gd").new()

func _ready():
	game_settings = get_tree().get_root().get_child(0).get_node("Settings/GameController")
	enemy_settings = get_tree().get_root().get_child(0).get_node("Settings/EnemySettings")

func control (delta):
	pass

func _physics_process (delta):
	if not alive:
		return
	
	control(delta)
	move_and_slide(velocity)

func body_rotate(target_position):
	$Body.look_at(target_position)
	$Hitbox.look_at(target_position)
	
func aim(arm, target_position):
	arm.look_at(target_position)

func take_damage(damage_value:float):
	self.health -= damage_value
	if self.health <= 0:
		die()

func die():
	alive = false
	self.queue_free()

func shoot():
	if equipped[0].can_shoot:
		equipped[0].shoot()
		equipped[0].can_shoot = false
		equipped[0].start_shot_timer()
	
	if weapon_type == 2: # dual  handed
		if equipped[1].can_shoot:
			equipped[1].shoot()
			equipped[1].can_shoot = false
			equipped[1].start_shot_timer()

func reload_guns() -> void:
	var full_reload_time = equipped[0].reload_timer + equipped[1].reload_timer
	for gun in equipped:
		gun.reload(full_reload_time)