extends "res://Scripts/Character/character.gd"

export (float) var roll_duration
export (float) var roll_cooldown
export (bool) var splitAim = false;

var isRolling = false;	
var current_target

func _ready():
	current_target = null

func control(delta):
	body_rotate(get_global_mouse_position())

	if (!splitAim):
		aim($Body/LeftArm, get_global_mouse_position())
	else:
		$Body.rotation_degrees = $Body.rotation_degrees - 90
		$Hitbox.rotation_degrees = $Body.rotation_degrees
		if ($Body.global_rotation > 0):
			$Body/LeftArm.global_rotation_degrees = abs($Body.global_rotation_degrees) - 90
		else:
			$Body/LeftArm.global_rotation_degrees = (abs($Body.global_rotation_degrees) + 90) * -1
	#RightArm Arm will always aim where you mouse is
	aim($Body/RightArm, get_global_mouse_position())
	
	var moveDir = Vector2();	
	
	if (Input.is_action_pressed("move_left")):
		moveDir.x = -1
	elif (Input.is_action_pressed("move_right")):
		moveDir.x = 1
	if (Input.is_action_pressed("move_upwards")):
		moveDir.y = -1
	elif (Input.is_action_pressed("move_downwards")):
		moveDir.y = 1

	if (Input.is_action_just_pressed("split_aim")):
		splitAim = !splitAim
	if (Input.is_action_pressed("fire")):
		shoot()
		
	velocity = moveDir * speed	
	targetting_enemy()
	
	if (Input.is_action_pressed("reload")):
		reload_guns()
	
func targetting_enemy() -> void:
	var aim_ray = get_node("Body/AimPoint")
	if aim_ray.is_colliding():
		
		var collision_object = aim_ray.get_collider()
		if collision_object.get_collision_layer() == 4: # enemy layer
			current_target = collision_object
			collision_object.set_aimed_at(true)

		else:
			if current_target != null:
				if is_instance_valid(current_target):
					current_target.set_aimed_at(false)
			current_target = null