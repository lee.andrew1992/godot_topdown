extends Position2D

func _ready():
	update_pivot_angle()
	
func _physics_process(delta):
	update_pivot_angle()
	
func update_pivot_angle():
	if (get_parent().splitAim):
		$Offset.position.x = 0
		$Offset/Camera2D.drag_margin_bottom = 0
		$Offset/Camera2D.drag_margin_left = 0
		$Offset/Camera2D.drag_margin_right = 0
		$Offset/Camera2D.drag_margin_top = 0
		rotation = get_parent().position.normalized().angle()
	else:
		$Offset.position.x = 200
		$Offset/Camera2D.drag_margin_bottom = 0.13
		$Offset/Camera2D.drag_margin_left = 0.13
		$Offset/Camera2D.drag_margin_right = 0.13
		$Offset/Camera2D.drag_margin_top = 0.13
		rotation = (get_global_mouse_position() - get_parent().position).normalized().angle()