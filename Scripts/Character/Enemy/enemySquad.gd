extends Node2D

var members
var player

func _ready():
	members = get_children()
	player = get_parent().get_node("Player")
	
func _process(delta):
	if (Input.is_action_just_pressed("debug_test")):
		for i in range(len(members)):
			if is_instance_valid(members[i]):
				members[i].get_cover(player, false)