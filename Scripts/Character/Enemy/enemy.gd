extends "res://Scripts/Character/character.gd"

var navigation
var curr_path : = PoolVector2Array() setget set_path

enum AI_STATE {
	following_order = 0,
	dodge = 1
}
export (AI_STATE) var state

enum ATTACK_ORDER {
	passive = 0,
	aggressive = 1
}

export (ATTACK_ORDER) var attack_order

enum PEEK_STATE {
	no_peek = 0,
	right_peek = 1,
	left_peek = 2
}
export (PEEK_STATE) var peek_state 

var target
var player
var moving : bool

var line_of_sight
var aim_point
var last_scene_position

var cover_node_manager
var current_cover_node
var occupying_nodes = []
var take_up_space = 50
var aimed_at

export (float) var dodge_duration = 3
export (float) var dodge_cooldown = 10
var can_dodge : bool

func _ready():
	cover_node_manager = get_tree().get_root().get_child(0).get_node("Map")
	navigation = get_tree().get_root().get_child(0).get_node("Map/Navigation") 
	player = get_parent().get_parent().get_node("Player")
	current_cover_node = null
	peek_state = PEEK_STATE.no_peek
	aimed_at = false
	$DodgeCooldownTimer.wait_time = dodge_cooldown
	$DodgeDuration.wait_time = dodge_duration
	can_dodge = true
	moving = false

func _draw():
	if game_settings.debug_mode:
		if line_of_sight:
			match peek_state:
				PEEK_STATE.no_peek:
					draw_line(Vector2(), aim_point - get_node("LineOfSight").global_position, Color(0,1,0), 1)
				PEEK_STATE.right_peek:
					draw_line(Vector2(), aim_point - get_node("LineOfSightRight").global_position, Color(0,1,0), 1)
				PEEK_STATE.left_peek:
					draw_line(Vector2(), aim_point - get_node("LineOfSightLeft").global_position, Color(0,1,0), 1)
		else:
			draw_line(Vector2(), player.get_global_position() - global_position, Color(1,0,0), 1)

func _process(delta):
	if curr_path.size() > 0:
		moving = true
	else:
		moving = false
	
	var aim_points_center = common_functions.target_points(get_node("LineOfSight"), player)
	
	if len(aim_points_center) == 0:
		var aim_points_left = common_functions.target_points(get_node("LineOfSightLeft"), player)
		var aim_points_right = common_functions.target_points(get_node("LineOfSightRight"), player)
		
		if len(aim_points_left) > 0 or len(aim_points_right) > 0:
			if len(aim_points_left) > len(aim_points_right):
				peek_state = PEEK_STATE.left_peek
				aim_point = aim_points_left[0]
				line_of_sight = true
			else:
				peek_state = PEEK_STATE.right_peek
				aim_point = aim_points_right[0]
				line_of_sight = true
		else:
			peek_state = PEEK_STATE.no_peek
			line_of_sight = false
	else:
		peek_state = PEEK_STATE.no_peek
		aim_point = aim_points_center[0]
		line_of_sight = true

	if aimed_at and !moving:
		if can_dodge:
			reactive_cover(player, dodge_duration, false)
	
	if equipped[0].empty() and equipped[1].empty() and not equipped[0].reloading and not equipped[1].reloading:
		reactive_cover(player, equipped[0].reload_timer + equipped[1].reload_timer, false)
		reload_guns()
	
	update()

func control (delta):
	var move_distance = speed * delta
	move_along_path(move_distance)
	body_rotate(player.global_position)
	
	if (aim_point != null):
		aim($Body/LeftArm, aim_point)
		aim($Body/RightArm, aim_point)
		
		if line_of_sight:
			shoot()
		else:
			if equipped[0].empty() and equipped[1].empty():
				reload_guns()

func _on_DetectionArea_body_entered(body):
	if body.get_collision_layer() == 2: # player layer
		target = body

func _on_DetectionArea_body_exited(body):
	if body == target:
		target = null
		line_of_sight = false
		last_scene_position = body.global_position
		
func sort_walls_by_distance(character):
	var walls = common_functions.all_walls(self)
	common_functions.sort_by_distance(character, walls)
	
	return walls

func create_paths(all_dest):
	var paths = []
	for destination in all_dest:
		var path = navigation.get_simple_path(global_position, destination.global_position)
		paths.append(path)
		
	return paths

func get_valid_cover_nodes(closest_walls, hide):
	var all_cover_nodes = []
	
	for wall in closest_walls:
		for node in (wall.cover_points + wall.hide_points):
			all_cover_nodes.append(node)
	
	var valid_nodes = []
	
	for node in all_cover_nodes:
		if is_instance_valid(node) and not node.occupied:
			if hide:
				if node.attack_score > 0 and node.attack_score < 3:
					valid_nodes.append(node)
			else:
				if node.attack_score == 0:
					valid_nodes.append(node)

	return valid_nodes

func set_path(path) -> void:
	if path != null:
		var dest_node = get_end_of_path_cover_node(path)
		if dest_node == null:
			return
		curr_path = path
		
		if path.size() == 0:
			return
		
		current_cover_node = dest_node
		release_nodes(dest_node)
		occupy_nodes(dest_node)
		set_process(true)
	
func occupy_nodes(current_node):
	for cover_node in cover_node_manager.all_node_array:
		if cover_node == current_node or cover_node.global_position.distance_to(current_node.global_position) <= take_up_space:
			cover_node.occupied = true
			cover_node.get_child(0).modulate = Color.black
			occupying_nodes.append(cover_node)

func release_nodes(current_node):
	for cover_node in occupying_nodes:
		cover_node.occupied = false
		cover_node.get_child(0).modulate = Color.red
	
	occupying_nodes.clear()

func move_along_path(distance : float) -> void:
	var start_point = position
	for index in range(curr_path.size()):
		var distance_to_next = start_point.distance_to(curr_path[0])
		if distance <= distance_to_next and distance >= 0.0:
			position = start_point.linear_interpolate(curr_path[0], distance / distance_to_next)
			break
		elif distance < 0.0:
			position = curr_path[0]
			set_process(false)
			moving = false
			break
		distance -= distance_to_next
		start_point = curr_path[0]
		curr_path.remove(0)

func reactive_cover(threat, duration, hide) -> void:
	var cover_obtained = get_cover(threat, hide)
	
	if cover_obtained and state == AI_STATE.dodge:
		$DodgeDuration.wait_time = duration
		$DodgeDuration.start()
		can_dodge = false

func get_cover(threat, hide) -> bool:
	var sorted_walls = sort_walls_by_distance(self)
	var closest_walls = common_functions.array_slice(sorted_walls, 0, enemy_settings.close_walls_optimization)
	
	for index in range(len(closest_walls)):
		closest_walls[index].get_possible_cover_nodes(threat)
	
	var dest_nodes = get_valid_cover_nodes(closest_walls, hide)
	if len(dest_nodes) == 0:
		return false
	else:
		state = AI_STATE.dodge
		common_functions.sort_by_distance(self, dest_nodes)
		dest_nodes = common_functions.array_slice(dest_nodes, 0, enemy_settings.path_optimization)
		var paths = create_paths(dest_nodes)
		common_functions.sort_path_by_safety(self, threat, paths, navigation)
		var safest_path = paths[0]
		set_path(safest_path)
		return true

func set_aimed_at(is_targetted):
	aimed_at = is_targetted

func _on_DodgeDuration_timeout():
	return_to_order()
	$DodgeDuration.stop()
	$DodgeCooldownTimer.start()

func return_to_order():
	state = AI_STATE.following_order
	match (attack_order):
		ATTACK_ORDER.aggressive:
			get_cover(player, false)
		ATTACK_ORDER.passive:
			get_cover(player, true)	

func _on_DodgeTimerCooldown_timeout():
	can_dodge = true

func get_end_of_path_cover_node(path):
	for node in cover_node_manager.all_node_array:
		if abs(node.global_position.x - path[-1].x) < 0.1 and abs(node.global_position.y - path[-1].y) < 0.1:
			return node