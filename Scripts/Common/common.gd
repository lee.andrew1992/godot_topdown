extends Node2D

enum SORT_MODE {
	SIMPLE = 0,
	PATH_DIST = 1,
	PATH_SAFETY = 2
}

var line_of_sight_node = load("res://Scenes/Characters/LineOfSight.tscn")

func target_points(current_object, player):
	var test = current_object.get_world_2d()
	var space_state = current_object.get_world_2d().direct_space_state

	var player_extents = player.get_node("Hitbox").shape.extents - Vector2 (2, 2)
	var player_point_nw = player.global_position - player_extents
	var player_point_ne = player.global_position + Vector2 (player_extents.x, -player_extents.y)
	var player_point_sw = player.global_position + Vector2 (-player_extents.x, player_extents.y)
	var player_point_se = player.global_position + player_extents
	
	var aim_points = []

	for pos in [player.global_position, player_point_nw, player_point_ne, player_point_sw, player_point_se]:
		var result = space_state.intersect_ray(current_object.global_position, pos, [current_object], current_object.collision_mask)
		if result:
			if result.collider.get_collision_layer() == 2: # player layer
				aim_points.append(pos)
	return aim_points
	
func all_walls(curr_object):
	var map_rooms = curr_object.get_tree().get_root().get_child(0).get_node("Map/Physics").get_children()
	var walls = []
	
	for room in map_rooms:
		for physic_node_child in room.get_children():
			if physic_node_child.get_child(0).get_collision_layer() == 16: # wall
				walls.append(physic_node_child)
			
	return walls
	
func sort_by_distance(origin_object, object_array):
	distance_quick_sort(origin_object, object_array, 0, len(object_array) - 1, SORT_MODE.SIMPLE)
	
func sort_by_nav_distance(origin_object, path_array, navigation_system):
	distance_quick_sort(origin_object, path_array, 0, len(path_array) - 1, SORT_MODE.PATH_DIST, navigation_system)

func sort_path_by_safety(origin_object, threat, path_array, navigation_system):
	distance_quick_sort(origin_object, path_array, 0, len(path_array) - 1, SORT_MODE.PATH_SAFETY, threat, navigation_system)

func distance_quick_sort(origin_object, array, low, high, sort_mode, threat = null, navigation_system = null):
	var partition_index = null
	if (low < high):
		if (navigation_system == null):
			partition_index = distance_quick_sort_partition(origin_object, array, low, high)
			distance_quick_sort(origin_object, array, low, partition_index - 1, sort_mode)
			distance_quick_sort(origin_object, array, partition_index + 1, high, sort_mode)	
		else:
			match sort_mode:
				SORT_MODE.PATH_DIST:
					partition_index = nav_distance_quick_sort_partition(origin_object, array, low, high, navigation_system)
				SORT_MODE.PATH_SAFETY:
					partition_index = path_safety_quick_sort_partition(origin_object, threat, array, low, high, navigation_system)
					
			distance_quick_sort(origin_object, array, low, partition_index - 1, sort_mode, threat, navigation_system)
			distance_quick_sort(origin_object, array, partition_index + 1, high, sort_mode, threat, navigation_system)

func distance_quick_sort_partition(origin_object, array, low, high):
	var partition_index = (low - 1)
	var pivot = array[high]
	
	var origin_position = origin_object.global_position
	
	for index in range(low, high):
		if array[index].global_position.distance_to(origin_position) < pivot.global_position.distance_to(origin_position):
			partition_index = partition_index + 1
			var temp_placement = array[partition_index]
			array[partition_index] = array[index]
			array[index] = temp_placement
	var temp_placement = array[partition_index + 1]
	array[partition_index + 1] = array[high]
	array[high] = temp_placement
	
	return partition_index + 1
	
func nav_distance_quick_sort_partition(origin_object, node_array, low, high, navigation_system):
	var partition_index = (low - 1)
	var pivot = node_array[high]
	
	var origin_position = origin_object.global_position
	
	for index in range(low, high):
		if (navigation_system.get_simple_path(origin_position, node_array[index].global_position)).size() < \
			(navigation_system.get_simple_path(origin_position, pivot.global_position)).size():
			partition_index = partition_index + 1
			var temp_placement = node_array[partition_index]
			node_array[partition_index] = node_array[index]
			node_array[index] = temp_placement
	var temp_placement = node_array[partition_index + 1]
	node_array[partition_index + 1] = node_array[high]
	node_array[high] = temp_placement
	
	return partition_index + 1
	
func path_safety_quick_sort_partition(origin_object, threat, path_array, low, high, navigation_system):
	var partition_index = (low - 1)
	var pivot = path_safety_points(path_array[high], origin_object, threat)
	
	var origin_position = origin_object.global_position
	
	for index in range(low, high):
		if (path_safety_points(path_array[index], origin_object, threat) < pivot):
			partition_index = partition_index + 1
			var temp_placement = path_array[partition_index]
			path_array[partition_index] = path_array[index]
			path_array[index] = temp_placement
	
	var temp_placement = path_array[partition_index + 1]
	path_array[partition_index + 1] = path_array[high]
	path_array[high] = temp_placement

	return partition_index + 1

func path_safety_points(path, origin_object, threat):
	var danger_points = 0
	for path_point in path:
		
		var temp_node = line_of_sight_node.instance()
		origin_object.get_parent().add_child(temp_node)
		temp_node.position = path_point
		danger_points += len(target_points(temp_node, threat))
		temp_node.free()
		
	return danger_points
	
func array_slice(array, from, to):
	var sliced_array = []
	
	if len(array) > to:
		for index in range(from, to):
			sliced_array.append(array[index])
	else:
		sliced_array = array
		
	return sliced_array